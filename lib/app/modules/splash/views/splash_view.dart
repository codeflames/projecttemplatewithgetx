import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:projecttemplatewithgetx/app/routes/app_pages.dart';

import '../controllers/splash_controller.dart';
import 'package:velocity_x/velocity_x.dart';

class SplashView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
        init: SplashController(),
        builder: (_) {
          return Scaffold(
            appBar: AppBar(
              title: Text('SplashView'),
              centerTitle: true,
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'SplashView is working',
                    style: TextStyle(fontSize: 20),
                  ),
                  ElevatedButton(
                      onPressed: _.getToHome,
                      child: "Go to Home view".text.make())
                ],
              ),
            ),
          );
        });
  }
}
