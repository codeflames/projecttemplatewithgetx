import 'package:get/get.dart';
import 'package:projecttemplatewithgetx/app/routes/app_pages.dart';

class SplashController extends GetxController {
  //TODO: Implement SplashController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void getToHome() {
    Get.toNamed(Routes.HOME);
  }
}
