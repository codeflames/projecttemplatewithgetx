import 'package:flutter/material.dart';

// THIS IS HOW THE COLORS IN THE APP WILL BE USED BASED ON THE DESIGN.
// TO CALL IT IS SIMPLER IN DEVELOPMENT

/* NOTE: these colors are not to be included in the intended project, it's just a template of how colors are to be placed */

const e4E4E6 = Color(0xFFE4E4E6);
const d60000 = Color(0xFFD60000);
const f2F2F2 = Color(0xFFF2F2F2);

const eight08080 = Color(0xFF808080);
const bFC4D5 = Color(0xFFBFC4D5);

const eB9A45 = Color(0xFFEB9A45);
const zeroE9F64 = Color(0xFF0E9F64);
const one8935F = Color(0xFF18935F);
const three43434 = Color(0xFF343434);
const d4D4D5 = Color(0xFFD4D4D5);
const one4764E = Color(0xFF14764E);

const zero0B46F = Color(0xFF00B46F);
const zero08753 = Color(0xFF008753);
const e7F7F0 = Color(0xFFE7F7F0);
const zero039D6 = Color(0xFF0039D6);
