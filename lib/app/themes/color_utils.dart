import 'dart:math' as math;

import 'package:flutter/material.dart';

enum ColorInt {
  WHITE,
  BLACK,
}

/// Determines whether the given [Color] is [Brightness.light] or
/// [Brightness.dark].
///
/// This compares the luminosity of the given color to a threshold value that
/// matches the material design specification.
Brightness estimateFlitaaBrightnessForColor(Color color) {
  final relativeLuminance = color.computeLuminance();

  // See <https://www.w3.org/TR/WCAG20/#contrast-ratiodef>
  // The spec says to use kThreshold=0.0525, but Material Design appears to bias
  // more towards using light text than WCAG20 recommends. Material Design spec
  // doesn't say what value to use, but 0.15 seemed close to what the Material
  // Design spec shows for its color palette on
  // <https://material.io/go/design-theming#color-color-palette>.
  const kThreshold = 0.5;
  if ((relativeLuminance + 0.05) * (relativeLuminance + 0.05) > kThreshold) {
    return Brightness.light;
  }
  return Brightness.dark;
}

ColorInt getContrastColor(Color color) {
  var whiteContrast = calculateContrast(Colors.white, color);
  var blackContrast = calculateContrast(Colors.black, color);

  return (whiteContrast > blackContrast) ? ColorInt.WHITE : ColorInt.BLACK;
}

double calculateContrast(Color foreground, Color background) {
  if (background.alpha != 255) {
    throw Exception('background can not be translucent: ${background.value}');
  }

  if (foreground.alpha < 255) {
    // If the foreground is translucent, composite the foreground over the background
    foreground = compositeColors(foreground, background);
  }
  final luminance1 = foreground.computeLuminance() + 0.05;
  final luminance2 = background.computeLuminance() + 0.05;
  // Now return the lighter luminance divided by the darker luminance
  return math.max(luminance1, luminance2) / math.min(luminance1, luminance2);
}

Color compositeColors(Color foreground, Color background) {
  var bgAlpha = background.alpha;
  var fgAlpha = foreground.alpha;
  var a = compositeAlpha(fgAlpha, bgAlpha);
  var r =
      compositeComponent(foreground.red, fgAlpha, background.red, bgAlpha, a);
  var g = compositeComponent(
      foreground.green, fgAlpha, background.green, bgAlpha, a);
  var b =
      compositeComponent(foreground.blue, fgAlpha, background.blue, bgAlpha, a);
  return Color.fromARGB(a, r, g, b);
}

int compositeAlpha(int foregroundAlpha, int backgroundAlpha) {
  return 0xFF - (((0xFF - backgroundAlpha) * (0xFF - foregroundAlpha)) ~/ 0xFF);
}

int compositeComponent(int fgC, int fgA, int bgC, int bgA, int a) {
  if (a == 0) return 0;
  return ((0xFF * fgC * fgA) + (bgC * bgA * (0xFF - fgA))) ~/ (a * 0xFF);
}

const int BRIGHTNESS_THRESHOLD = 130;

/// Calculate whether a color is light or dark, based on a commonly known
/// brightness formula.
///
/// @see {@literal http://en.wikipedia.org/wiki/HSV_color_space%23Lightness}
///
bool isColorDark(Color color) {
  return ((30 * color.red + 59 * color.green + 11 * color.blue) / 100) <=
      BRIGHTNESS_THRESHOLD;
}
