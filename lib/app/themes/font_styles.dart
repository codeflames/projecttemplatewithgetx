// import 'package:flutter/material.dart';

// import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// import '../../r.g.dart';
// import 'colors.dart';

/*


THIS IS AN EXAMPLE OF HOW THE TEXTS IN THE PROJECT ARE TO BE STYLED

 */

// final _fallBacks = <String>[R.fontFamily.roboto];

double _platformFontSize(double size) {
  /* print(ScreenUtil().pixelRatio);
  if (Get.find<DbService>().fontScalingEnabled.isTrue ||
      ScreenUtil().pixelRatio > 2.7) {
    return size.sp;
  } else {
    return (size+2).sp;
  } */
  return size.sp;
}

// bool get isBigIos {
//   return Device.get().isIos && Device.get().isTablet;
// }

// bool get isIosWithNotch {
//   return Device.get().isIos && Device.get().hasNotch;
// }

// final avenirHeavy20 = TextStyle(
//   fontFamily: R.fontFamily.avenir,
//   // // color: blumine,
//   height: 1.32.h,
//   fontSize: _platformFontSize(20),
//   fontWeight: FontWeight.bold,
//   fontFamilyFallback: _fallBacks,
// );

// /// // colors.white
// final avenirHeavy24 = TextStyle(
//   fontFamily: R.fontFamily.avenir,
//   // color: // colors.white,
//   height: 1.35.h,
//   fontSize: _platformFontSize(24),
//   fontWeight: FontWeight.bold,
//   fontFamilyFallback: _fallBacks,
// );

// // dolphin
// final avenirMedium13 = TextStyle(
//   // color: dolphin,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(13),
//   height: 1.20.h,
//   fontWeight: FontWeight.w500,
//   fontFamilyFallback: _fallBacks,
// );

// final avenirDemi11 = TextStyle(
//   color: two4B57A,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(11),
//   height: 1.14.h,
//   fontWeight: FontWeight.w600,
//   fontFamilyFallback: _fallBacks,
// );

// final avenirDemi12 = TextStyle(
//   color: fourB4F52,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(12),
//   height: 1.16.h,
//   fontWeight: FontWeight.w600,
//   fontFamilyFallback: _fallBacks,
// );

// /// // color: astronaut,
// final avenirDemi13 = TextStyle(
//   // color: astronaut,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(13),
//   height: 1.20.h,
//   fontWeight: FontWeight.w600,
//   fontFamilyFallback: _fallBacks,
// );

// /// // color: astronaut,
// final avenirDemi14 = TextStyle(
//   // color: fourB4F52,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(14),
//   height: 1.22.h,
//   fontWeight: FontWeight.w600,
//   fontFamilyFallback: _fallBacks,
// );

// /// // color: astronaut,
// final avenirBold13 = TextStyle(
//   // color: astronautBluish,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(13),
//   height: 1.20.h,
//   fontWeight: FontWeight.w800,
//   fontFamilyFallback: _fallBacks,
// );

// final avenirMedium10 = TextStyle(
//   // color: persianBlue,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(10),
//   height: 1.14.h,
//   fontWeight: FontWeight.w500,
//   fontFamilyFallback: _fallBacks,
// );

// /// black
// final avenirMedium12 = TextStyle(
//   color: Colors.black,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(12),
//   height: 1.18.h,
//   fontWeight: FontWeight.w500,
//   fontFamilyFallback: _fallBacks,
// );

// ///
// final avenirMedium14 = TextStyle(
//   // color: astronaut,
//   fontFamily: R.fontFamily.avenir_next,
//   fontSize: _platformFontSize(14),
//   height: 1.28.h,
//   fontWeight: FontWeight.w500,
//   fontFamilyFallback: _fallBacks,
// );

// final avenirMedium16 = TextStyle(
//   fontFamily: R.fontFamily.avenir_next,
//   fontWeight: FontWeight.w500,
//   height: 1.22.h,
//   // color: astronautBluish,
//   fontSize: _platformFontSize(16),
//   fontFamilyFallback: _fallBacks,
// );

// final avenirMedium40 = TextStyle(
//   fontFamily: R.fontFamily.avenir_next,
//   fontWeight: FontWeight.w500,
//   height: 1.56.h,
//   color: three43434,
//   fontSize: _platformFontSize(40),
//   fontFamilyFallback: _fallBacks,
// );

// /// white
// final avenirDemi16 = TextStyle(
//   fontFamily: R.fontFamily.avenir_next,
//   fontWeight: FontWeight.w600,
//   height: 1.22.h,
//   color: Colors.white,
//   fontSize: _platformFontSize(16),
//   fontFamilyFallback: _fallBacks,
// );

// /// #262626
// final avenirDemi18 = TextStyle(
//   fontFamily: R.fontFamily.avenir_next,
//   fontWeight: FontWeight.w600,
//   height: 1.24.h,
//   color: two62626,
//   fontSize: _platformFontSize(18),
//   fontFamilyFallback: _fallBacks,
// );

// /// // color: abbey,
// final avenirHeavy16 = TextStyle(
//   fontFamily: R.fontFamily.avenir,
//   fontWeight: FontWeight.w800,
//   height: 1.23.h,
//   // color: abbey,
//   fontSize: _platformFontSize(16),
//   fontFamilyFallback: _fallBacks,
// );
