import 'package:flutter/material.dart';

// THIS CLASS TAKES CARE OF THE APP THEME...IT WILL BE
// CALLED ON THE MAIN.

class AppTheme {
  static ThemeData mainTheme = ThemeData.light().copyWith(
    scaffoldBackgroundColor: Colors.white,
    primaryColor: Colors.white,

    // THE FONT FAMILY WILL BE GOTTEN FROM THE INTENDED PROJECT DESIGN FILE

    // textTheme: TextTheme().apply(fontFamily: R.fontFamily.avenir_next),
    // primaryTextTheme: TextTheme().apply(fontFamily: R.fontFamily.avenir_next),
  );
}
