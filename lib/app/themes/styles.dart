import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:pull_to_refresh/pull_to_refresh.dart';

/*
THIS IS HOW THE BORDER AND BUTTON STYLES WILL BE PLACED IN THE PROJECT
 */

// Radii
final borderRadius4 = BorderRadius.circular(4.r);
final borderRadius8 = BorderRadius.circular(8.r);
final borderRadius12 = BorderRadius.circular(12.r);
final borderRadius16 = BorderRadius.circular(16.r);

final borderRadiusLR8 = BorderRadius.only(
  topLeft: Radius.circular(8.r),
  topRight: Radius.circular(8.r),
);

final RoundedRectangleBorder roundBox =
    RoundedRectangleBorder(borderRadius: borderRadius4);

final RoundedRectangleBorder roundBox8 =
    RoundedRectangleBorder(borderRadius: borderRadius8);

final RoundedRectangleBorder roundBox16 =
    RoundedRectangleBorder(borderRadius: borderRadius16);

// Shadow
// List<BoxShadow> getShadow({
//   Color color = threeF3F4419,
//   double offsetX = 0,
//   double offsetY = 1,
//   double? blur,
// }) {
//   return [
//     BoxShadow(
//       color: color,
//       offset: Offset(offsetX, offsetY),
//       blurRadius: blur ?? 8.r,
//     )
//   ];
// }

final shadow4 = [
  BoxShadow(color: Color(0xFFEBEBEC), offset: Offset(0, 1), blurRadius: 4.r)
];

// decoration
final whiteRound = BoxDecoration(color: Colors.white, shape: BoxShape.circle);

final lightShadow = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(4.r),
  boxShadow: [
    BoxShadow(color: Color(0xFFEBEBEC), offset: Offset(0, 1), blurRadius: 5.r)
  ],
);

final shimmerContainer = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(6.r),
);

final roundList = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(16.r),
);

final bottomSheetDecoration = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.horizontal(
    left: Radius.circular(8.r),
    right: Radius.circular(8.r),
  ),
);

final decorationWithShadow = BoxDecoration(
  borderRadius: borderRadius4,
  boxShadow: shadow4,
  color: Colors.white,
);

// final decorationWithDarkerShadow = BoxDecoration(
//   borderRadius: borderRadius4,
//   boxShadow: getShadow(color: Colors.black12),
// );

// final outlineInputBorder = OutlineInputBorder(
//   borderRadius: borderRadius4,
//   borderSide: BorderSide(
//     color: dEDEDF,
//     width: 1.w,
//   ),
// );

// final focusedBorder = outlineInputBorder.copyWith(
//   borderSide: BorderSide(color: Colors.black),
// );

// final inputDecoration = InputDecoration(
//   floatingLabelBehavior: FloatingLabelBehavior.always,
//   border: outlineInputBorder,
//   enabledBorder: outlineInputBorder,
//   focusedBorder: focusedBorder,
//   labelStyle: avenirMedium12.copyWith(fontSize: 18.sp, color: sevenA809B),
//   contentPadding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 16.w),
//   hintStyle: avenirMedium14.copyWith(color: six46A86.withOpacity(0.55)),
//   filled: true,
//   fillColor: Colors.white,
// );

// // Button Style
// final blackButton = ElevatedButton.styleFrom(
//   padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 16.w),
//   primary: Colors.black,
//   // onPrimary: Colors.white,
//   shadowColor: zero0000019,
//   textStyle: avenirDemi16.copyWith(color: Colors.white),
//   elevation: 1,
//   shape: roundBox,
// );

// final smallTextButton = TextButton.styleFrom(
//   primary: Colors.black,
//   textStyle: avenirMedium16.copyWith(decoration: TextDecoration.underline),
// );

// final smallerTextButton = TextButton.styleFrom(
//   primary: Colors.black,
//   textStyle: avenirMedium13.copyWith(decoration: TextDecoration.underline),
// );

// final smallRedTextButton = TextButton.styleFrom(
//   primary: fF6A6A,
//   textStyle: avenirMedium13.copyWith(decoration: TextDecoration.underline),
// );

// final numberButton = TextButton.styleFrom(
//   padding: EdgeInsets.symmetric(vertical: 24.h, horizontal: 16.w),
//   textStyle: avenirHeavy20,
//   primary: two62626,
//   backgroundColor: Colors.white,
// );

// final paymentButton = TextButton.styleFrom(
//   primary: Colors.black,
//   shape: RoundedRectangleBorder(
//     borderRadius: BorderRadius.circular(16.r),
//   ),
//   padding: EdgeInsets.all(16.w),
//   backgroundColor: e9E9E9,
//   textStyle: avenirMedium10.copyWith(
//     decoration: TextDecoration.underline,
//   ),
// );

// final shrunkButton = TextButton.styleFrom(
//   backgroundColor: cDEBE0,
//   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(11.r)),
//   padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 8.w),
//   tapTargetSize: MaterialTapTargetSize.shrinkWrap,
//   minimumSize: Size.zero,
// );

// final walletButton = TextButton.styleFrom(
//   primary: Colors.black,
//   padding: EdgeInsets.zero,
//   tapTargetSize: MaterialTapTargetSize.shrinkWrap,
//   textStyle: avenirHeavy16,
//   shape: RoundedRectangleBorder(borderRadius: borderRadius16),
// );

// final settingsButton = TextButton.styleFrom(
//   primary: Colors.black,
//   padding: EdgeInsets.symmetric(horizontal: 4.w),
//   tapTargetSize: MaterialTapTargetSize.shrinkWrap,
// );

// final maxButtonStyle = TextButton.styleFrom(
//   primary: fBB56C,
//   textStyle: avenirMedium13,
// );

// Padding
final paddingV24H20 = EdgeInsets.symmetric(horizontal: 20.w, vertical: 24.h);

final paddingH20 = EdgeInsets.symmetric(horizontal: 20.w);

final paddingH51 = EdgeInsets.symmetric(horizontal: 51.w);

final paddingV16H12 = EdgeInsets.symmetric(vertical: 16.h, horizontal: 12.w);

final paddingV16H20 = EdgeInsets.symmetric(vertical: 16.h, horizontal: 20.w);

final paddingH12 = EdgeInsets.symmetric(horizontal: 12.w);

// Refresh indicator
// final defaultHeader = defaultTargetPlatform == TargetPlatform.iOS
//     ? ClassicHeader()
//     : MaterialClassicHeader(color: Colors.black);

// final LoadIndicator defaultFooter = ClassicFooter();
